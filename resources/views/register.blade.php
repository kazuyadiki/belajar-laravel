<!DOCTYPE html>
<html>
<head>
<title>Form</title>
</head>
<body>

<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="POST">
    @csrf
    <label for="fname">First Name:</label><br><br>
    <input type="text" id="fname" name="fname"><br><br>
    <label for="lname">Last Name:</label><br><br>
    <input type="text" id="lname" name="lname"><br><br>

    <label>Gender:</label><br><br>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label type="other">Other</label><br><br>

    <label>Nationality:</label><br><br>
    <select>
    <option value="Indonesian">Indonesian</option>
    <option value="Malaysian">Malay</option>
    <option value="Japanese">Japanese</option>
    <option value="Canadian">Canadian</option>
    </select><br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
    <label for="language1">Bahasa Indonesia</label><br>
    <input type="checkbox" id="language2" name="language2" value="English">
    <label for="language2">English</label><br>
    <input type="checkbox" id="language3" name="language3" value="Other">
    <label for="language3">Other</label><br><br>

    <label>Bio:</label><br><br>
    <textarea name="Bio" rows="10" cols="30"></textarea><br>
    <input type="submit" value="Sign Up">
</form>    
</body>
</html>